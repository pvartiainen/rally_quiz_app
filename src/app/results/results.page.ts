import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { RallyQuestion } from '../../rallyquestion';

@Component({
  selector: 'app-results',
  templateUrl: './results.page.html',
  styleUrls: ['./results.page.scss'],
})
export class ResultsPage implements OnInit {

  //Member variables
  questions: RallyQuestion[] = [];
  activeQuestion: RallyQuestion;
  isCorrect: boolean;
  feedback: string;
  questionCounter: number;
  optionCounter: number;

 
 correctCount: number;
  
  //variables
  duration: number;
  durationSeconds: number;
  oikeavastaus: number;
  feedbacks: string;


  constructor(public router: Router, public activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
    this.durationSeconds = Math.round((this.duration) / 1000);

    this.oikeavastaus = Number(this.activatedRoute.snapshot.paramMap.get('oikeavastaus'));
    this.feedback = this.activatedRoute.snapshot.paramMap.get('feedbacks');

    fetch('../../assets/data/data.ts').then(res => res.json())
    .then(json => {
      this.questions = json;
      //console.log(this.questions.length);
      //this.setQuestion();
    });

  }

  goAnswers() {
    this.router.navigateByUrl('answers');
  }
  
   
 checkOption(option: number, activeQuestion: RallyQuestion) {
  this.optionCounter++;
  if (this.optionCounter > activeQuestion.options.length) {
  }
  if (option === Number(activeQuestion.correctOption)) {
    this.isCorrect = true;
    
    this.feedback =
    ' Oikein! Paina ralliautoa kerran jatkaaksesi!'
  } else {
    this.isCorrect = false;
    this.feedback = ''
    }
}
 

}
