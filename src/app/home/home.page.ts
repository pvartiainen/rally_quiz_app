import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  data: any;

  constructor(public router: Router) {}

  ngOnInit() {
  }
  goQuestions() {
    this.router.navigateByUrl('questions');
  }

}
