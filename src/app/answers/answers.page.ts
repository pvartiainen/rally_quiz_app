import { Component, OnInit } from '@angular/core';
import { RallyQuestion } from '../../rallyquestion';
import { Router } from '@angular/router';


@Component({
  selector: 'app-answers',
  templateUrl: './answers.page.html',
  styleUrls: ['./answers.page.scss'],
})
export class AnswersPage implements OnInit {


   //Member variables
   questions: RallyQuestion[] = [];
   activeQuestion: RallyQuestion;
   isCorrect: boolean;
   feedback: string;
   questionCounter: number;
   optionCounter: number;
 
   startTime: Date;
   endTime: Date;
   duration: number;

   data: any;

  constructor(public router: Router) { }

  ngOnInit() {

    fetch('../../assets/data/data.ts').then(res => res.json())
    .then(json => {
      this.questions = json;
      //console.log(this.questions.length);
      this.setQuestion();
    });

    this.activeQuestion = this.questions[this.questionCounter];
    //this.questionCounter++;
    this.startTime = new Date();

    this.questionCounter = 0;   
   }

   setQuestion() {
    if (this.questionCounter === this.questions.length) {
      this.endTime = new Date();
      this.duration = this.endTime.getTime() - this.startTime.getTime();
      this.router.navigateByUrl('results/' + this.duration);
      this.ngOnInit();
    } else {
    this.optionCounter = 0;
    this.feedback = '';
    this.isCorrect = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
  }
}
   
 checkOption(option: number, activeQuestion: RallyQuestion) {
  this.optionCounter++;
  if (this.optionCounter > activeQuestion.options.length) {
    this.setQuestion();
  }
  if (option === Number(activeQuestion.correctOption)) {
    this.isCorrect = true;
    this.feedback =
    ' Oikein! Paina ralliautoa kerran!';
  } else {
    this.isCorrect = false;
    this.feedback = 'Väärin.'
    this.setQuestion();
    
  }
}

goHome() {
  this.router.navigateByUrl('home');
}
 

}
