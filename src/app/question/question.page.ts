import { Component, OnInit } from '@angular/core';
import { RallyQuestion } from '../../rallyquestion';
import { Router } from '@angular/router';


@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {

   //Member variables
   questions: RallyQuestion[] = [];
   activeQuestion: RallyQuestion;
   isCorrect: boolean;
   feedback: string;
   questionCounter: number;
   optionCounter: number;
   oikeavastaus: number;
   feedbacks: string;
 
   startTime: Date;
   endTime: Date;
   duration: number;
   correctCount: number;

   data: any;
 
  
   constructor(public router: Router) {}

   ngOnInit() {

    fetch('../../assets/data/data.ts').then(res => res.json())
    .then(json => {
      this.questions = json;
      //console.log(this.questions.length);
      this.setQuestion();
    });

    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
    this.startTime = new Date();
    this.questionCounter = 0;  
    this.oikeavastaus = 0;
   }

   setQuestion() {
    if (this.questionCounter === this.questions.length) {
      this.endTime = new Date();
      this.duration = this.endTime.getTime() - this.startTime.getTime();
      this.router.navigateByUrl('results/' + this.duration +
      '/' + this.oikeavastaus +
      '/' + this.feedbacks);
      this.ngOnInit();
    } else {
    this.optionCounter = 0;
    this.feedback = '';
    this.isCorrect = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
  }
}
   
 checkOption(option: number, activeQuestion: RallyQuestion) {
  this.optionCounter++;
  if (this.optionCounter > activeQuestion.options.length) {
    this.setQuestion();
  }
  if (option === Number(activeQuestion.correctOption)) {
    this.isCorrect = true;
    this.oikeavastaus ++;
    if(this.oikeavastaus <= 3) {
      this.feedbacks = 'Palaute: Vielä jäi parannettavaa seuraavaan kertaan :)'
    } else {
      this.feedbacks = 'Palaute: Hienosti meni!'
    }
    //alert(this.oikeavastaus); <-- Tämä testiä varten.
    this.feedback =
    ' Oikein! Paina ralliautoa kerran jatkaaksesi!'
  } else {
    this.isCorrect = false;
    this.feedback = ''
    this.setQuestion();
    }55
}
 
}
