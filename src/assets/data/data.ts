[
  {
    "quote": "Kuka heistä ajaa M-Sport WRC -autolla vuonna 2019?",
    "options": [
      "Sebastien Ogier",
      "Elfyn Evans",
      "Esapekka Lappi"
    ],
      "correctOption": "1"
    },
    {
      "quote": "Kuka voitti rallin SM -sarjan SM1 luokassa vuonna 2019?",
      "options": [
        "Teemu Asunmaa",
        "Emil Lindholm",
        "Juha Salo"
      ],
        "correctOption": "0"
    },
    {
      "quote": "Mikä ralli käynnistää rallin mm-sarjan vuonna 2020?",
      "options": [
        "Ruotsi",
        "Meksiko",
        "Monte-Carlo"
      ],
        "correctOption": "2"
    },
    {
      "quote": "Mistä voit nähdä MM-sarjan jokaisen erikoiskokeen livenä?",
      "options": [
        "Yle",
        "Red Bull TV",
        "WRC All live"
      ],
        "correctOption": "2"
    },
    {
      "quote": "Kuka on tällä hetkellä tiukimmin kiinni vuoden 2019 rallin WRC luokan maailmanmestaruudessa?",
      "options": [
        "Kris Meeke",
        "Sebastien Ogier",
        "Ott Tänak"
      ],
        "correctOption": "2"
    },
    {
      "quote": "Kuka on norjan suurin rallilupaus tällä hetkellä?",
      "options": [
        "Oliver Solberg",
        "Andreas Mikkelsen",
        "Mads Östberg"
      ],
        "correctOption": "0"
    }
]